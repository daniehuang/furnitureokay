# README #

FurnitureOkay is a Melbourne-based retailer specialised in offering high quality outdoor furniture in-store and online. All our products are designed for living and manufactured from a range of durable materials, including handwoven synthetic wicker, travertine stone, powder-coated aluminium and ceramic glass. Whether you choose a patio recliner, a lounge suite, a dining setting, a daybed or anything else at all, you can have full peace of mind knowing you are buying from the outdoor furniture specialist.

https://www.furnitureok.com.au